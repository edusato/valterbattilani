<section id="conteudopagina">
	<div class="container">
		<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10 pd0">
			<!-- // -->
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInLeft">
				<div class="box-curso">
					<div class="thumb-curso">
						<img class="filterimg" src="timthumb.php?src=assets/images/thumb01.jpg&w=413&h=290&q=100" alt="">
					</div>
					<div class="desc-curso">
						<h2>Nullam Molestie In Libero Eget Consectetur.</h2>
						<p>Aliquam sit amet justo in orci tempus tristique at et massa. Sed dignissim, mi nec sollicitudin porta, lacus purus rhoncus lorem, volutpat luctus tortor odio et eros. Integer sollicitudin quis quam a hendrerit. Aliquam sit amet enim facilisis, convallis nisi sagittis, lacinia libero. Morbi condimentum odio vel arcu volutpat mollis.</p>
					</div>
					<div class="botoes-box-curso">
						<a href="?pag=curso"><i class="far fa-plus-square"></i> Mais Informações</a>
						<a class="popupColorbox" href="#form-modal"><i class="far fa-calendar-alt"></i> Agendar</a>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInLeft">
				<div class="box-curso">
					<div class="thumb-curso">
						<img class="filterimg" src="timthumb.php?src=assets/images/thumb02.jpg&w=413&h=290&q=100" alt="">
					</div>
					<div class="desc-curso">
						<h2>Nullam Molestie In Libero Eget Consectetur.</h2>
						<p>Aliquam sit amet justo in orci tempus tristique at et massa. Sed dignissim, mi nec sollicitudin porta, lacus purus rhoncus lorem, volutpat luctus tortor odio et eros. Integer sollicitudin quis quam a hendrerit. Aliquam sit amet enim facilisis, convallis nisi sagittis, lacinia libero. Morbi condimentum odio vel arcu volutpat mollis.</p>
					</div>
					<div class="botoes-box-curso">
						<a href="?pag=curso"><i class="far fa-plus-square"></i> Mais Informações</a>
						<a class="popupColorbox" href="#form-modal"><i class="far fa-calendar-alt"></i> Agendar</a>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInLeft">
				<div class="box-curso">
					<div class="thumb-curso">
						<img class="filterimg" src="timthumb.php?src=assets/images/thumb03.jpg&w=413&h=290&q=100" alt="">
					</div>
					<div class="desc-curso">
						<h2>Nullam Molestie In Libero Eget Consectetur.</h2>
						<p>Aliquam sit amet justo in orci tempus tristique at et massa. Sed dignissim, mi nec sollicitudin porta, lacus purus rhoncus lorem, volutpat luctus tortor odio et eros. Integer sollicitudin quis quam a hendrerit. Aliquam sit amet enim facilisis, convallis nisi sagittis, lacinia libero. Morbi condimentum odio vel arcu volutpat mollis.</p>
					</div>
					<div class="botoes-box-curso">
						<a href="?pag=curso"><i class="far fa-plus-square"></i> Mais Informações</a>
						<a class="popupColorbox" href="#form-modal"><i class="far fa-calendar-alt"></i> Agendar</a>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInLeft">
				<div class="box-curso">
					<div class="thumb-curso">
						<img class="filterimg" src="timthumb.php?src=assets/images/thumb04.jpg&w=413&h=290&q=100" alt="">
					</div>
					<div class="desc-curso">
						<h2>Nullam Molestie In Libero Eget Consectetur.</h2>
						<p>Aliquam sit amet justo in orci tempus tristique at et massa. Sed dignissim, mi nec sollicitudin porta, lacus purus rhoncus lorem, volutpat luctus tortor odio et eros. Integer sollicitudin quis quam a hendrerit. Aliquam sit amet enim facilisis, convallis nisi sagittis, lacinia libero. Morbi condimentum odio vel arcu volutpat mollis.</p>
					</div>
					<div class="botoes-box-curso">
						<a href="?pag=curso"><i class="far fa-plus-square"></i> Mais Informações</a>
						<a class="popupColorbox" href="#form-modal"><i class="far fa-calendar-alt"></i> Agendar</a>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInLeft">
				<div class="box-curso">
					<div class="thumb-curso">
						<img class="filterimg" src="timthumb.php?src=assets/images/thumb01.jpg&w=413&h=290&q=100" alt="">
					</div>
					<div class="desc-curso">
						<h2>Nullam Molestie In Libero Eget Consectetur.</h2>
						<p>Aliquam sit amet justo in orci tempus tristique at et massa. Sed dignissim, mi nec sollicitudin porta, lacus purus rhoncus lorem, volutpat luctus tortor odio et eros. Integer sollicitudin quis quam a hendrerit. Aliquam sit amet enim facilisis, convallis nisi sagittis, lacinia libero. Morbi condimentum odio vel arcu volutpat mollis.</p>
					</div>
					<div class="botoes-box-curso">
						<a href="?pag=curso"><i class="far fa-plus-square"></i> Mais Informações</a>
						<a class="popupColorbox" href="#form-modal"><i class="far fa-calendar-alt"></i> Agendar</a>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInLeft">
				<div class="box-curso">
					<div class="thumb-curso">
						<img class="filterimg" src="timthumb.php?src=assets/images/thumb02.jpg&w=413&h=290&q=100" alt="">
					</div>
					<div class="desc-curso">
						<h2>Nullam Molestie In Libero Eget Consectetur.</h2>
						<p>Aliquam sit amet justo in orci tempus tristique at et massa. Sed dignissim, mi nec sollicitudin porta, lacus purus rhoncus lorem, volutpat luctus tortor odio et eros. Integer sollicitudin quis quam a hendrerit. Aliquam sit amet enim facilisis, convallis nisi sagittis, lacinia libero. Morbi condimentum odio vel arcu volutpat mollis.</p>
					</div>
					<div class="botoes-box-curso">
						<a href="?pag=curso"><i class="far fa-plus-square"></i> Mais Informações</a>
						<a class="popupColorbox" href="#form-modal"><i class="far fa-calendar-alt"></i> Agendar</a>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInLeft">
				<div class="box-curso">
					<div class="thumb-curso">
						<img class="filterimg" src="timthumb.php?src=assets/images/thumb03.jpg&w=413&h=290&q=100" alt="">
					</div>
					<div class="desc-curso">
						<h2>Nullam Molestie In Libero Eget Consectetur.</h2>
						<p>Aliquam sit amet justo in orci tempus tristique at et massa. Sed dignissim, mi nec sollicitudin porta, lacus purus rhoncus lorem, volutpat luctus tortor odio et eros. Integer sollicitudin quis quam a hendrerit. Aliquam sit amet enim facilisis, convallis nisi sagittis, lacinia libero. Morbi condimentum odio vel arcu volutpat mollis.</p>
					</div>
					<div class="botoes-box-curso">
						<a href="?pag=curso"><i class="far fa-plus-square"></i> Mais Informações</a>
						<a class="popupColorbox" href="#form-modal"><i class="far fa-calendar-alt"></i> Agendar</a>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow fadeInLeft">
				<div class="box-curso">
					<div class="thumb-curso">
						<img class="filterimg" src="timthumb.php?src=assets/images/thumb04.jpg&w=413&h=290&q=100" alt="">
					</div>
					<div class="desc-curso">
						<h2>Nullam Molestie In Libero Eget Consectetur.</h2>
						<p>Aliquam sit amet justo in orci tempus tristique at et massa. Sed dignissim, mi nec sollicitudin porta, lacus purus rhoncus lorem, volutpat luctus tortor odio et eros. Integer sollicitudin quis quam a hendrerit. Aliquam sit amet enim facilisis, convallis nisi sagittis, lacinia libero. Morbi condimentum odio vel arcu volutpat mollis.</p>
					</div>
					<div class="botoes-box-curso">
						<a href="?pag=curso"><i class="far fa-plus-square"></i> Mais Informações</a>
						<a class="popupColorbox" href="#form-modal"><i class="far fa-calendar-alt"></i> Agendar</a>
					</div>
				</div>
			</div>
			<!-- // -->
		</div>
	</div>
</section>