<section id="conteudopagina" class="bgwhite">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10 capacurso wow fadeInLeft">
				<img class='filterimg' src="assets/images/thumb-curso.jpg" width="100%">
			</div>
			<div class="col-xs-12 col-sm-12 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6 descricao-curso wow fadeInRight">
				<h1>Nullam molestie in libero eget consectetur.</h1>
				<p>Curabitur ac sodales turpis. Maecenas ut risus tempor, luctus sapien sit amet, fermentum eros. Ut rutrum quam nec ullamcorper auctor. In mollis sem ligula, nec lacinia neque porttitor ac. Cras malesuada risus lectus, vitae convallis turpis viverra ac. Cras a dignissim felis. Integer ornare mi ac justo tincidunt pulvinar.</p>

				<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-offset-3 col-md-3 col-lg-offset-3 col-lg-3 data-horario wow fadeInLeft">
				<i class="far fa-clock"></i>
				<span>Duração</span><br/>
				<strong>08 Horas</strong>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 data-horario wow fadeInLeft">
				<i class="far fa-calendar-alt"></i>
				<span>Datas disponíveis</span><br/>
				<strong>01 à 29 de julho</strong><br/>
				<small>*verificar detalhes através do contato</small>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 btn-agendar-curso wow fadeInRight">
				<a class="popupColorbox" href="#form-modal"><i class="far fa-calendar-alt"></i> Agendar</a>
			</div>
		</div>
	</div>
</section>