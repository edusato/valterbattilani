<div class="hidden">
	<div id="form-modal">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h2>Para agendar o curso, preencha os campos abaixo:</h2>
			</div>
			<form action="">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<input type="text" name="nome" placeholder="Nome">
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<input type="text" name="email" placeholder="E-mail para contato">
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<input type="text" name="telefone" placeholder="Telefone para contato">
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<textarea name="msg" placeholder="Escrever Informações adicionais"></textarea>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<button type="submit"><i class="far fa-calendar-check"></i> AGENDAR</button>
				</div>
			</form>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<p>Ao clicar em agendar será enviado um e-mail para a equipe de atentimento do Dr. Battilani. Aguarde o contato da equipe para confirmar o agendamento e obter informações sobre preços e formas de pagamento.</p>
			</div>
		</div>
	</div>
</div>

<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-offset-1 col-md-2 col-lg-offset-1 col-lg-2 wow fadeInLeft">
				<img src="assets/images/logo.png" height="30px">
			</div>
			<div class="col-xs-12 col-sm-4 col-md-offset-3 col-md-3 col-lg-offset-3 col-lg-3 wow fadeInRight">
				<span>CENTRO ODONTOLÓGICO BATTILANI</span>
				<a href="#"><i class="fab fa-facebook-f"></i></a>
				<a href="#"><i class="fab fa-instagram"></i></a>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-2 col-lg-2 wow fadeInRight">
				<span>VALTER BATTILANI</span>
				<a href="#"><i class="fab fa-linkedin-in"></i></a>
				<a href="#"><i class="fab fa-facebook-f"></i></a>
				<a href="#"><i class="fab fa-instagram"></i></a>
			</div>
		</div>
	</div>	
</footer>
</section><!-- SECTION SITE -->
<!-- scripts -->
<script src="assets/scripts/components/modernizr/modernizr.js" type="text/javascript"></script>
<script src="assets/scripts/components/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="assets/scripts/components/colorbox/jquery.colorbox-min.js" type="text/javascript"></script>
<script src="assets/scripts/components/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/scripts/components/bxslider-4/src/js/jquery.bxslider.js" type="text/javascript"></script>
<script src="assets/scripts/components/bxslider-4/src/vendor/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="assets/scripts/components/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="assets/scripts/components/wow/dist/wow.min.js"></script>
<script src="assets/scripts/components/app.js" type="text/javascript"></script>
<?php //include('controller.php'); ?>
</body>
</html>