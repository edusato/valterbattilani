<section id="conteudopagina">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-offset-1 col-md-5 col-lg-offset-1 col-lg-5 wow fadeInLeft">
				<img class='filterimg img-responsive' src="assets/images/valter-battilani.jpg">
			</div>
			<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5 wow fadeInRight">
				<h1 class='titulopag'>Valter Battilani</h1>
				<p>Curabitur ac sodales turpis. Maecenas ut risus tempor, luctus sapien sit amet, fermentum eros. Ut rutrum quam nec ullamcorper auctor. In mollis sem ligula, nec lacinia neque porttitor ac. Cras malesuada risus lectus, vitae convallis turpis viverra ac. Cras a dignissim felis. Integer ornare mi ac justo tincidunt pulvinar.</p>

				<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
			</div>
		</div>
	</div>
</section>