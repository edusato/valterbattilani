<section id="conteudopagina">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-offset-1 col-md-5 col-lg-offset-1 col-lg-5 wow fadeInLeft">
				<div id="formulario-contato">
					<h1>Como posso ajudar?</h1>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pd0c">
						<input type="text" name="nome" placeholder="NOME">
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pd0c">
						<input type="text" name="email" placeholder="E-MAIL">
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pd0c">
						<input type="text" name="assunto" placeholder="ASSUNTO">
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pd0c">
						<textarea name="msg" placeholder="MENSAGEM"></textarea>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pd0c">
						<input type="submit" value="ENVIAR">
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5 wow fadeInRight" style="position: relative;">
				<div class="infocontato">
					<p>Atendimento<br/>
					<strong>Segunda a Sexta das 8h às 12h - 13h30 às 18h</strong></p>

					<h3>45 98402-3154</h3>

					<p>contato@drbattilani.com.br</p>

					<p>R. Santa Catarina, 1265 - 4 - Centro,<br/>Cascavel - PR, 85801-040</p>
				</div>
				<img class='filterimg' src="assets/images/valter-battilani.jpg" width="100%">
			</div>
		</div>
	</div>
</section>